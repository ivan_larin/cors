import axios from "axios";
import { WeatherForecast } from "../DataContracts/WeatherForecast";

export class WeatherForecastApi {
    static async get(): Promise<WeatherForecast[]> {
        return (await axios.get('https://localhost:44306/WeatherForecast')).data;
    }
}