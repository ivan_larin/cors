import { useEffect, useState } from 'react';
import styles from './styles.module.css';
import { WeatherForecast } from './DataContracts/WeatherForecast';
import { WeatherForecastApi } from './Api/WeatherForecastApi';
import moment from 'moment';

function App() {

  const [weather, setWeather] = useState<WeatherForecast[]>([]);

  useEffect(() => {
    (async () => {
      const fetched = await WeatherForecastApi.get();
      setWeather(fetched);
    })();
  }, []);

  return (
    <div className={styles.weatherList}>
      {weather.map(item => 
        <div className={styles.weather}>
          <div>{moment(item.date).format('DD.MM.YYYY')}</div>
          <div>{item.temperatureC} °C</div>
          <div>{item.temperatureF} °F</div>
          <div>{item.summary}</div>
        </div>
        )}
    </div>
  );
}

export default App;
